<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title><?php echo isset($title) ? 'Alertsz - '.$title : 'Alertsz'; ?></title>
<link rel="stylesheet" href="css/main.css">
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>