<?php
$title = 'Privacy';
include('header.php'); 
?>
<div class="alertsz-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<header class="header">
					<a class="brand" href="/">Alertsz</a>
				</header>
				<div class="jumbotron">
					<h1>Privacy policy</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-8">
			<div class="content">
				<p><small class="text-muted">Last updated January 8, 2014</small></p>
				<p>Alertsz, LLC ("Alertsz" "us" or "we") is committed to respecting the privacy of our users. This privacy policy (the "Privacy Policy") is intended to inform you of our policies and practices regarding the collection, use and disclosure of any information you submit to us through the Alertsz website (the "Website"). If you have any questions about our privacy policy, please email us at contact@SoRewarding.com.</p>
				<p>User Consent<br>
					PLEASE READ THIS PRIVACY POLICY CAREFULLY.</p>
				<p>By accessing or using our Website, you acknowledge that you have read, understood, and agree to be bound to all the terms of this Privacy Policy.</p>
				<p><b>Changes to Privacy Policy</b></p>
				<p>The Alertsz Privacy Policy may change from time to time. Any revised privacy policy will be consistent with our services. If we make material changes to this policy, we will notify you here, by email, or by means of notice on our home page.</p>
				<p><b>Collection and Use of Information by Alertsz</b></p>
				<p>This Privacy Policy discusses "Personal Information" which is information from which your identity is discernable, such as your photo, name, email address, home or work address, and telephone number, and "Non-Personal Information" which is information not associated with or linked to your Personal Information, such as your zip code, age, gender and preferences. If you choose to interact with our Website and/or mobile application by, for example, creating a profile, posting content such as a notification, sending us feedback, or applying for a job, we will ask you to provide certain Personal Information.</p>
				<p>We may also gather Non-Personal Information about your use of Alertsz, such as your device hardware type and model, the areas of the website you visit, the services you access, location data and software such as your operating system version, IP address, browser type and version, domain names, access times and referring Web site addresses.</p>
				<p>If you choose to use our referral service (invite friends or others) to tell a friend about Alertsz or refer a job posting to a friend or third party ("your referral"), we will ask you for the name and email address of your referral. We will automatically send your referral a one-time email inviting him or her to visit Alertsz or view the subject material. We do not store this information and we do not disseminate this information in violation of this Privacy Policy.</p>
				<p><b>Use of Your Information by Alertsz</b></p>
				<p>The Personal Information you submit to us is used to provide you with access to Alertsz, to help us communicate with you, to improve Alertsz services and develop new applications.  If you have provided consent for us to do so, we may inform you of other products or services available from us or our partners. We may use your information to send you emails or other communications regarding updates at Alertsz, to seek your opinion on current products and services or possible new products and services that may be offered. We may send you emails on new Alertsz opportunities and additional job postings which may be of interest to you. The nature and frequency of these messages will vary depending upon the information we have about you.</p>
				<p>Some profile such as your name and photo may be viewable by other users in which you have included in your Alertsz circles.</p>
				<p>We may also use your Personal Information to trouble shoot, resolve disputes, accomplish administrative tasks, contact you, comply with applicable law and cooperate with law enforcement activities, and to enforce our agreements with you including our Terms and Conditions and this Privacy Policy.</p>
				<p>We use Non-personal Information to troubleshoot, administer the Website, analyze trends, gather demographic information, comply with applicable law, and cooperate with law enforcement activities.</p>
				<p><b>Release of Personal Information to Others</b></p>
				<p>We do not disclose your Personal Information to third parties, except as set forth below.</p>
				<p>We may share your Personal Information with authorized third party service providers who perform functions on our behalf. These third party service providers will have access to your Personal Information as necessary to perform their functions, but they may not share that information with any other third party without violating their agreements of confidentiality with us.</p>
				<p>We will disclose Personal Information if legally required to do so, if requested to do so by a governmental entity, court of law, competent subpoena authority or if we believe in good faith that such action is necessary to: (a) conform to legal requirements or comply with legal process; (b) protect our rights or property or that of our affiliated companies; (c) prevent a crime or protect national security; or (d) protect the personal safety of Alertsz users or the public.</p>
				<p>We will disclose and transfer Personal Information to a third party who acquires all or a substantial portion of our business, whether such acquisition is by way of merger, consolidation or purchase of all or a substantial portion of our assets. In addition, in the event we become the subject of a bankruptcy proceeding, whether voluntary or involuntary, we or our trustee in bankruptcy may sell, license or otherwise dispose of such Personal Information in a transaction approved by the bankruptcy court. You will be notified of sale of all or a substantial portion of our business to a third party via email or through a prominent notice posted on the Alertsz website.</p>
				<p><b>Release of Non-Personal Information to Others</b></p>
				<p>We may share aggregated Non-personal Information about users of the Alertsz Website with our partners, businesses, and other third parties so that they can understand the nature and type of users of Alertsz.</p>
				<p>Information gathered on a co-branded page (such as that of a contest co-sponsored by Alertsz and another company), may become the property of the other company or of both us and the other company. In that instance, the use of such information by the other company will be subject to the privacy policy of that company. We are not responsible for that company's use of your personal or demographic information in violation of this policy or any similar privacy policy of their own.</p>
				<p><b>Your Choices Regarding Your Personal Information</b></p>
				<p>You may choose not to provide Personal Information. This will limit your ability to engage in certain activities on the Website and/or mobile application.</p>
				<p>When you receive promotional communications from us, you may indicate a preference to stop receiving further promotional communications from us and you will have the opportunity to "opt-out" by following the unsubscribe instructions provided in the promotional email you receive.</p>
				<p>Despite your indicated email preferences, we may send you administrative emails regarding Alertsz, including for example, administrative confirmations, and notices of updates to our Privacy Policy.</p>
				<p><b>Third Party Sites and Advertising</b></p>
				<p>Alertsz may contain links to other websites. Please be aware that we are not responsible for the privacy practices or the content of such other websites. We encourage our users to read the privacy statements of each website they visit. This Privacy Policy applies solely to information collected by us through Alertsz and does not apply to these third party websites. The ability to access information of third parties from Alertsz or links to other websites is for your convenience only and does not signify our endorsement of such third parties, their products, services, other websites, locations or their content.</p>
				<p><b>Posting to Public Areas of Alertsz</b></p>
				<p>If you post Personal Information in public areas of the Alertsz website or mobile application, such as in online notifications, or in the Alertsz database, such information may be collected and used by others over whom we have no control. We are not responsible for the use made by third parties of information you post or otherwise make available in public areas of Alertsz and under no circumstance shall Alertsz be held accountable for any loss, damage or injury you might suffer as a result thereof.</p>
				<p><b>Use of Cookies</b>. We use "cookies" to help personalize and maximize your online experience and time online. A cookie is a text file that is placed on your hard drive by a Web page server. Cookies are not used to run programs or deliver viruses to your computer. One of the primary purposes of cookies is to provide a convenience feature to save you time. For example, if you personalize Alertsz pages, or register for services, a cookie helps us to recall your specific information (such as user name, password and preferences). Because of our use of cookies, we can deliver faster and more accurate results and a more personalized site experience. When you return to Alertsz, the information you previously provided can be retrieved, so you can easily use the features you have customized.</p>
				<p>You may have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline all cookies if you prefer. Alternatively, you may be able to modify your browser setting to notify you each time a cookie is tendered and permit you to accept or decline cookies on an individual basis. If you choose to decline cookies, however, that may hinder performance and negatively impact your experience on Alertsz.</p>
				<p><b>Use of Web Beacons</b></p>
				<p>Alertsz may contain electronic images known as Web beacons (sometimes called single-pixel gifs) that allow us to count users who have visited those pages and to deliver co-branded services. Web beacons are not used to access your personally identifiable information; they are a technique we use to compile aggregated statistics about our Web site usage. Web beacons collect only a limited set of information including a cookie number, time and date of a page view, and a description of the page on which the Web beacon resides.</p>
				<p>You may not decline web beacons, however, they can be rendered ineffective by declining all cookies or modifying your browser setting to notify you each time a cookie is tendered and permit you to accept or decline cookies on an individual basis.</p>
				<p>Third parties are not permitted to use Web beacons on Alertsz.</p>
				<p><b>Access to and Modification of Your Information</b></p>
				<p>You may view and edit the Personal information you provide to us at any time by logging into your profile.</p>
				<p>We retain indefinitely all the information we gather about you in an effort to make your repeat use of Alertsz more efficient and relevant. You can delete your profile from the Alertsz online database and close your Alertsz account at any time, in which event we will remove all our copies of your information from Alertsz and remove your Alertsz account, except for an archival copy which is not accessible on the Internet.</p>
				<p><b>Security of Personal Information</b></p>
				<p>You can access your Personal Information on our Website through a password and your email address. This password is encrypted. We recommend that you do not share your password with anyone. When you place orders or access your personal account information, you are utilizing secure server software SSL which encrypts your personal information before it is sent over the Internet.</p>
				<p>We have implemented commercially reasonable technical and organizational measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration or disclosure. However, we cannot guarantee that unauthorized third parties will never be able to defeat those measures or use your personal information for improper purposes. Alertsz shall not, under any circumstance, be liable for any unauthorized, illegal or impermissible use by a third party of your personal information.</p>
				<p><b>Children</b></p>
				<p>Minors under the age of 15 may not use the Website. We do not collect or maintain information from anyone know to be under the age of 15, and no part of the Website is designed to attract anyone under the age of 15.</p>
				<p><b>Good Neighbor Policy</b></p>
				<p>Alertsz is a web 2.0 enabled site based upon community. We are dedicated to keeping Alertsz content appropriate for all members of the community. In the terms of service, all users agree to avoid infringing on copyrighted, trademarked, licensed, intellectual property and other protected material unlawfully. Similarly, Alertsz mandates that users and others visiting Alertsz refrain from any use, conduct or activity for the purpose of which would or might be to promote or express hate or discrimination against anyone or any group for any reason, or which might be construed as harassment, inappropriate comments or unlawful behavior of any type. Everyone has different viewpoints, and we need to remain tolerant of other views as well as do our best to avoid antagonizing others. In order to get along on the World Wide Web, we all need to be good neighbors. Comments, posts, or users who are found in violation of the terms of service or the good neighbor policy and/or who violates the terms and conditions of use for Alertsz will suffer corrective action including account revocation and will be subject to civil and criminal penalties applicable in such circumstances.</p>
				<p>Notice of Privacy Rights to California Residents. California law requires that we provide you with a summary of your privacy rights under the California Online Privacy Protection Act (the "Act") and the California Business and Professions Code. As required by the Act, we will provide you with the categories of Personally Identifiable Information that we collect through the Website and the categories of third party persons or entities with whom such Personally Identifiable Information may be shared for direct marketing purposes at your request. California law requires us to inform you, at your request, (1) the categories of Personally Identifiable Information we collect and what third parties we share that information with; (2) the names and addresses of those third parties; and (3) examples of the products marketed by those companies. The Act further requires us to allow you to control who you do not want us to share that information with. To obtain this information, please send a request by email or standard mail to the address found below. When contacting us, please indicate your name, address, email address, and what Personally Identifiable Information you do not want us to share with Affiliated Businesses or Marketing Partners. The request should be sent to the attention of our legal department, and labeled "California Customer Choice Notice." Please allow 30 days for a response. Also, please note that there is no charge for controlling the sharing of your Personally Identifiable Information or requesting this notice.</p>
				<p><b>Contact Information</b></p>
				<p>Corporate Address:</p>
				<address>Alertsz, LLC<br>
					1660 S. Albion Street <br>
					Suite #207, Denver, CO 80222</address>
				<p>For customer service inquires: <a href="mailto:contact@Alertsz.com">contact@Alertsz.com</a></p>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>