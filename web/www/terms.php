<?php
$title = 'Terms of use';
include('header.php'); 
?>
<div class="alertsz-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<header class="header">
					<a class="brand" href="/">Alertsz</a>
				</header>
				<div class="jumbotron">
					<h1>Terms of use</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-8">
			<div class="content">
				<p><small class="text-muted">Last Modified January 9, 2014</small></p>
				<p>Please read and follow the Terms and Conditions for using the Alertsz website (the “Website”) and mobile application (the “App”).  By accessing and utilizing the services and information provided through the Website/App, you agree to follow and be bound by the Terms and Conditions as listed herein.  If you do not agree to these Terms and Conditions do not use the Website/App.</p>
				<p><b>The Terms of Service May Be Modified</b></p>
				<p>The Alertsz website and mobile application is owned by Alertsz, LLC and Alertsz reserves the right to modify these Terms and Conditions at any time.  Changes in law or third party partnerships may constitute a change in Terms.  Any change in Terms will be effective at the time they are posted.  You should check the Terms for updates and discontinue the use of the Website and mobile application if you no longer agree.</p>
				<p><b>Using Alertsz Services</b></p>
				<p>Alertsz is designed to send and receive location based notifications in real-time to other community members for the purpose of sharing information pertinent to your surroundings or events that happen near you.  Emergency notifications may be sent or received by verified government agencies as well as people within in the community.  It is your responsibility to use the Services in a manner consistent with the design of the Alertsz website and/or mobile application.  You are responsible for the content of your post and any consequences due to the result of your content.</p>
				<p>You must follow the policies of the Alertsz service.</p>
				<p>Do not abuse or misuse the services provided.</p>
				<p>Do not spam users.</p>
				<p>Do not post information known to be false.</p>
				<p>Do not post private information in public areas.</p>
				<p>Do not post inappropriate material.</p>
				<p>Respect the rights of other users.</p>
				<p>You must not attempt to gain unauthorized access to any part of the Alertsz website or mobile application.</p>
				<p>Do not attempt to collect the information of other users by the use of automated bots or any manual means.  Harvesting bots, robots, spiders or scrapers may not be used.</p>
				<p><b>License</b></p>
				<p>Alertsz grants you a personal, limited, non-exclusive, non-transferable, non-sub licensable license to use the software and Services for personal and non-commercial purposes.  This license does not allow for any resale or commercial use of the Services provided.  This license allows you to use and benefit from the Services which are provided.</p>
				<p><b>Your Account</b></p>
				<p>You are responsible for protecting your password and restricting access to your account via your computer and/or mobile device.  You accept responsibility for any activity which occurs under your account.</p>
				<p><b>Alertsz Rights and Limitations of Liability</b></p>
				<p>All rights, title and interest in the Services are the exclusive property of Alertsz LLC.  These Services are protected by copyright, trademark, patent and other laws of both the United States and foreign countries.  The users have no right to use any of the protected trademarks, logos, domain names or distinctive feature of the Alertsz brand.</p>
				<p>Alertsz reserves the right to block or disallow access to users who abuse the Services.</p>
				<p>The Alertsz service is provided to you “AS IS and “AS AVAILABLE”.  You use the Services provided at your own risk.</p>
				<p>Your use of the Alertsz Services is not a replacement for any law enforcement agencies or emergency services such as “911”.</p>
				<p>Alertsz does not guarantee alerts/messages are received or sent as a variety of conditions can exist that can contribute to messaging failure (i.e. network issues, drops in coverage, device failure, etc.).</p>
				<p>By installing the application or using the website, user/community agrees to hold harmless Alertsz for any loss, personal injury, damage or legal liability though use of the web site or App ,including but not limited to, actions taken by the user or others as the result of receiving/sending an alert/message.  Do not use Alertsz or the Alertsz mobile application in a situation that would put you in harm’s way.   Do not use the mobile application while driving or operating equipment that could bring harm to others or property.</p>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>