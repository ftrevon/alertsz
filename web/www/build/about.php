<?php
$title = 'About Alertsz';
include('header.php'); 
?>
<div class="alertsz-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<header class="header">
					<a class="brand" href="/">Alertsz</a>
				</header>
				<div class="jumbotron">
					<h1>About Alertsz</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-8">
			<div class="content">
				<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, dignissimos sunt maiores blanditiis esse repellendus cumque consectetur alias commodi nulla ex laborum distinctio necessitatibus eveniet facere deserunt itaque voluptas fuga.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, natus, minus, laudantium pariatur numquam sapiente reiciendis suscipit voluptas earum eligendi enim saepe vitae nesciunt iusto debitis quae a dignissimos. Atque, dignissimos esse mollitia temporibus modi natus perspiciatis omnis quo molestias inventore? Laudantium, earum sint quae sequi consequuntur reiciendis quo. Nemo, distinctio, beatae, illo libero rem molestias assumenda iure id at error doloribus impedit sapiente non voluptatum fugiat perspiciatis culpa provident omnis harum vero odio laudantium qui ducimus repudiandae consequuntur numquam eligendi in doloremque dolores velit. Nobis, praesentium, sint architecto ab sunt aspernatur necessitatibus facere delectus voluptatum rem asperiores dignissimos voluptatem.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, tenetur, consectetur similique omnis itaque architecto eligendi quaerat culpa quis error consequuntur facilis laudantium sit aut assumenda reiciendis ducimus nam possimus sint suscipit ullam officiis et numquam fugit nobis quos nisi est eveniet obcaecati exercitationem excepturi ea at dolores delectus dolorem sunt perferendis. Quisquam, beatae, quod architecto debitis commodi voluptate corrupti obcaecati facere eveniet vero porro earum quam ea quas explicabo ex harum? Aut, praesentium necessitatibus incidunt unde labore earum officiis fugiat laboriosam odit aperiam explicabo vero quaerat illo. Animi, officiis distinctio possimus ex odit eum laudantium deserunt quam eligendi non a voluptatum sint assumenda impedit modi corporis earum omnis maxime recusandae aliquid architecto cumque in facere repudiandae ut inventore pariatur quaerat asperiores dolor incidunt dolore molestias quia est minus soluta quod veritatis iste reiciendis blanditiis consequatur temporibus culpa obcaecati veniam ipsum nobis eius facilis dolorum id odio laborum! Optio, molestias!</p>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>