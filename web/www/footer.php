<div class="alertsz-bottom">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8">
				<a href="#" class="get-app">available on the app store</a>
				<span class="qr">qr code</span>
			</div>	
			<div class="col-lg-4 col-md-3 col-md-offset-1 col-lg-offset-0">
				<div class="patent">
					patent pending
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(isset($page)&&$page=='home'){ ?>
<div class="how-it-works" id="learn">
	<div class="container">
		<h2>3 types of Notifications</h2>
		<div class="row">
			<div class="col-lg-4 col-md-4 notification-type">
				<i class="icon bounce icon-post ion-ios7-compose-outline"></i>
				<h3>Posts</h3>
				<p>Non-emergency posts are used to share information with your circles</p>
			</div>
			<div class="col-lg-4 col-md-4 notification-type">
				<i class="icon bounce bounce-b icon-sighting ion-eye"></i>
				<h3>Sightings</h3>
				<p>Report issues that may impact others in your community</p>
			</div>
			<div class="col-lg-4 col-md-4 notification-type">
				<i class="icon bounce bounce-c icon-emergency ion-alert-circled"></i>
				<h3>Emergency</h3>
				<p>If you find yourself or others in imminent danger, alert your contacts and those around you</p>
			</div>
		</div>
	</div>
	<div class="features text-left">
		<div class="container">
			<h2 class="text-center">Use Aletsz to</h2>
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="feature">
						<i class="icon ion-iphone"></i>
						<p>Notify in real-time the people you know, those near your current location, and the community, in case of emergencies</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="feature">
						<i class="icon ion-map"></i>
						<p>Monitor physical addresses of interest to be alerted of emergencies, sightings or anything suspicious or danger around those locations</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="feature">
						<i class="icon ion-help-buoy"></i>
						<p>Alert and receive immediate help form people close to your current physical location in times of personal crisis or emergencies</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="feature">
						<i class="icon ion-speakerphone"></i>
						<p>Receive information, instructions and updates from local, state and federal organizations in the event of crisis</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="feature">
						<i class="icon ion-ios7-people"></i>
						<p>Send real-time vital updates to specific recipients in times of crisis (employees, students parent, etc.)</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<nav class="navbar" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#alertsz-nav">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	  	<div class="collapse navbar-collapse" id="alertsz-nav">
			<ul class="nav navbar-nav">
				<li><span>&copy; <?php echo date("Y"); ?> Alertsz llc</span></li>
				<li><a href="about.php">About</a></li>
				<li><a href="terms.php">Terms</a></li>
				<li><a href="privacy.php">Privacy</a></li>
			</ul>
		</div>
	</div>
</nav>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script type="text/javascript">
$(function(){
	$('#alertsz-features').carousel('cycle');
	$('.alertsz-carousel').on('slide.bs.carousel', function(ev) {
		var dir = ev.direction == 'right' ? 'prev' : 'next';
    	$('.alertsz-carousel').not('.sliding').addClass('sliding').carousel(dir);
	});
	$('.alertsz-carousel').on('slid.bs.carousel', function(ev) {
		$('.alertsz-carousel').removeClass('sliding');
	});
	$('#learn').waypoint(function() {
		$('#learn').addClass('active');
	}, { offset: 400 });
});
</script>
</body>
</html>