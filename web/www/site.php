<?php
$title = 'Awesome Tagline';
$page = 'home';
include('header.php'); 
?>
<div class="alertsz-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<header class="header">
					<a class="brand" href="/">Alertsz</a>
				</header>
				<div class="jumbotron">
					<h1>Crowd Security<br> for the Community</h1>
					<p>Alertsz is your 21st century neighborhood watch. Let your community know what's happening. Get notified about people and places that matter to you.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-5">
				<div class="screens">
					<div id="alertsz-screens" class="carousel slide alertsz-carousel">
						<div class="carousel-inner">
							<div class="item active">
								<img src="images/screens/feed.jpg" alt="feed screen">
							</div>
							<div class="item">
								<img src="images/screens/circles.jpg" alt="circles screen">
							</div>
							<div class="item">
								<img src="images/screens/feed.jpg" alt="feed screen">
							</div>
							<div class="item">
								<img src="images/screens/profile.jpg" alt="profile screen">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-3 col-sm-6">
				<div id="alertsz-features" class="carousel vertical slide alertsz-features alertsz-carousel">
					<ol class="carousel-indicators">
						<li data-target="#alertsz-features" data-slide-to="0" class="active"></li>
						<li data-target="#alertsz-features" data-slide-to="1"></li>
						<li data-target="#alertsz-features" data-slide-to="2"></li>
						<li data-target="#alertsz-features" data-slide-to="3"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<h2><i class="icon ion-wifi"></i> Feed</h2>
							<p>fugiat minus facere vitae totam repellendus ex quos aliquam possimus magni nostrum esse recusandae </p>
						</div>
						<div class="item">
							<h2><i class="icon ion-android-friends"></i> Circles</h2>
							<p>fugiat minus facere vitae totam repellendus ex quos aliquam possimus magni nostrum esse recusandae </p>
						</div>
						<div class="item">
							<h2><i class="icon ion-eye"></i> Sightings</h2>
							<p>fugiat minus facere vitae totam repellendus ex quos aliquam possimus magni nostrum esse recusandae </p>
						</div>
						<div class="item">
							<h2><i class="icon ion-ios7-contact-outline"></i> Profile</h2>
							<p>fugiat minus facere vitae totam repellendus ex quos aliquam possimus magni nostrum esse recusandae </p>
						</div>
					</div>
				</div>
				<a href="#learn" class="learn-more"><i class="icon ion-arrow-down-c"></i> learn more</a>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>